gitlab page https://gitlab.com/userAA/cloud-storage-gitlab-english-comments.git
gitlab comment cloud-storage-gitlab-english-comments

project cloud-storage
technologies used on the frontend:
    @ant-design/icons,
    @types/node,
    @types/react,
    @types/react-dom,
    antd,
    axios,
    cookies,
    next,
    nookies,
    react,
    react-dom,
    sass,
    typescript;

technologies used on the backend:
    @nestjs/common,
    @nestjs/config,
    @nestjs/core,
    @nestjs/jwt,
    @nestjs/mapped-types,
    @nestjs/passport,
    @nestjs/platform-express,
    @nestjs/swagger,
    @nestjs/typeorm,
    @types/multer,
    @types/passport-jwt,
    @types/passport-local,
    express,
    passport,
    passport-jwt,
    passport-local,
    pg,
    reflect-metadata,
    rxjs,
    typeorm,
 
    @nestjs/cli,
    @nestjs/schematics,
    @nestjs/testing,
    @types/express,
    @types/jest,
    @types/node,
    @types/supertest,
    @typescript-eslint/eslint-plugin,
    @typescript-eslint/parser,
    eslint,
    eslint-config-prettier,
    eslint-plugin-prettier,
    jest,
    prettier,
    source-map-support,
    supertest,
    ts-jest,
    ts-loader,
    ts-node,
    tsconfig-paths,
    typescript


User registration and user authorization is carried out. 
An authorized user can upload files and send them to the trash.