import React from "react";
import styles from "./FileList.module.scss";
import { fileItem } from "@/api/dto/files.dto";
import { FileCard } from "@/components/FileCard";
import Selecto from "react-selecto";

export type FileSelectType = "select" | "unselect";

interface FileListProps {
    //template for a list of all files of an authorized user
    items: fileItem[];
    //the function of highlighting any file of an authorized user
    onFileSelect: (id: number, type: FileSelectType) => void;
}

//the component of displaying all files of an authorized user
export const FileList: React.FC<FileListProps> = ({ items, onFileSelect }) => {
    return (
        <div className={styles.root}>
            {items.map((item) => (
                //showing each file of an authorized user
                <div data-id={item.id} key={item.id} className="file">
                    <FileCard filename={item.filename} originalName={item.originalName} />
                </div>
            ))}

            {/*file selection component*/}
            <Selecto
                container=".files"
                selectableTargets={[".file"]}
                selectByClick
                hitRate={10}
                selectFromInside
                toggleContinueSelect={["shift"]}
                continueSelect={false}
                onSelect={(e) => {
                    e.added.forEach((el) => {
                        el.classList.add("active");
                        onFileSelect(Number(el.dataset["id"]), "select");
                    });
                    e.removed.forEach((el) => {
                        el.classList.remove("active");
                        onFileSelect(Number(el.dataset["id"]), "unselect");
                    });
                }}
            />
        </div>
    );
};