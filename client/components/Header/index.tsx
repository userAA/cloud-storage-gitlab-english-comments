import React from "react";
import {Layout, Avatar, Menu, Popover, Button} from "antd";
import styles from "./Header.module.scss";
import {CloudOutlined} from "@ant-design/icons";
//вытаскиваем хук useRouter
import { useRouter } from "next/router";

import * as Api from "@/api";

export const Header: React.FC = () => {
    //defining the router using the useRouter hook
    const router = useRouter();
    //we define the router of the selected main menu key
    const selectedMenu = router.pathname;

    //handler of the authorization withdrawal event from an authorized user
    const onClickLogout = () => {
        if (window.confirm("Do you really want to get out?"))
        {
            //we carry out the specified event
            Api.auth.logout();
            //go to the user authorization page
            location.href = "/dashboard/auth";
        }
    }

    return (
        <Layout.Header className={styles.root}>
            <div className={styles.headerInner}>
                <div className={styles.headerLeft}>
                    <h2>
                        <CloudOutlined />
                        Cloud Storage
                    </h2>
                
                    <Menu
                        className={styles.topMenu}
                        theme="dark"
                        mode="horizontal"
                        defaultSelectedKeys={[selectedMenu]}
                        onSelect = {({key}) => router.push(key)}
                        items = {[
                            //the key to go to the main page of the project
                            { key: "/dashboard", label: "Main"},
                            //the key to go to the page showing information about the authorized user
                            { key: "/dashboard/profile", label: "Profile"}
                        ]}
                    />
                </div>

                {/*the component of removing authorization from an authorized user */}
                <div className={styles.headerRight}>
                    <Popover
                        trigger="click"
                        content = {
                            //the button for removing authorization from an authorized user
                            <Button onClick={onClickLogout} type="primary" danger>
                                Escape
                            </Button>
                        }
                    >
                        <Avatar>A</Avatar>
                    </Popover>
                </div>
            </div>
        </Layout.Header>
    )
}