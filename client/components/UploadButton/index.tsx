import React from "react"
import styles from "@/styles/Home.module.scss";
import {Upload, Button, UploadFile, notification} from "antd";
import {CloudUploadOutlined} from "@ant-design/icons";

import * as Api from "@/api";

//the component of uploading information about a new file of an authorized user to the server
export const UploadButton: React.FC = () => {
    const [fileList, setFileList] = React.useState<UploadFile[]>([]);

    //function of the specified file operation
    const onUploadSuccess = async (options : any) => {
        try
        {
            //we send the selected file to the server
            const file = await Api.files.uploadFile(options);
            setFileList([]);
            window.location.reload();
        }
        catch (err)
        {
            //it was not possible to send the selected file to the server
            notification.error({
                message: "Error!",
                description: "Failed to upload file",
                duration: 2
            });
        }
    }

    return (
        //the component of downloading a file from disk and sending its information to the server
        <Upload
            customRequest={onUploadSuccess}
            fileList={fileList}
            onChange={({fileList}) => setFileList(fileList)}
            className={styles.upload}
        >
            {/*the button for downloading a file from disk and sending its information to the server*/}
            <Button type="primary" icon={<CloudUploadOutlined/>} size="large">
                Upload a file
            </Button>
        </Upload>
    )
}