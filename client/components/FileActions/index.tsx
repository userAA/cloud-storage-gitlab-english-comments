import React from "react";
import styles from "./FileActions.module.scss";
import { Button, Popconfirm } from "antd";

interface FileActionsProps {
    //the function of deleting selected files
    onClickRemove: VoidFunction,
    //flag indicating that there are dedicated files
    isActive: boolean
}

//component of actions with selected files of an authorized user
export const FileActions: React.FC<FileActionsProps> = ({
    onClickRemove,
    isActive
}) => {
    return (
        <div className={styles.root}>
            {/*The component of sending selected files to the trash*/}
            <Popconfirm
                title="Delete file(s)"
                description="All files will be moved to the trash"
                okText="Yes"
                cancelText="No"
                disabled={!isActive}
                onConfirm={onClickRemove}
            >
                {/*The function of sending selected files to the trash*/}
                <Button disabled={!isActive} type="primary" danger>
                    Remove
                </Button>
            </Popconfirm>
        </div>
    )
}