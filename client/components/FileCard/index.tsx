import React from "react"
import styles from "./FileCard.module.scss"
import {getExtensionFromFileName} from "@/utils/getExtensionFromFileName" 
import { getColorByExtension} from "@/utils/getColorByExtension"
import {FileTextOutlined} from "@ant-design/icons"

//data template for each file of an authorized user
interface FileCardProps {
    //the name of the file on the server
    filename: string;
    //original file name
    originalName: string;
}

//the component of displaying any file of an authorized user
export const FileCard: React.FC<FileCardProps> = ({originalName, filename}) => {
    //we define the extension of each file of the authorized user
    const ext = getExtensionFromFileName(filename);
    
    //we determine whether some file of an authorized user is an image, if so, we determine its url
    const imageUrl = ext && ['jpg', 'jpeg', 'png', 'bmp'].includes(ext) ? "http://localhost:7777/uploads/" + filename : "";

    const color = getColorByExtension(ext);
    
    //we determine the color with which we mark the extension of the authorized user's image file
    const classColor = styles[color];

    return (
        <div className={styles.root}>
            <div className={styles.icon}>
                <i className={classColor}>{ext}</i>
                { ['jpg', 'jpeg', 'png', 'bmp'].includes(ext) ? (
                    //showing an image file of an authorized user
                    <img className={styles.image} src={imageUrl} alt="File" />
                ) : (
                    //template for displaying an authorized user's text file
                    <FileTextOutlined/>
                )}
            </div>
            {/*Output the initial name of each file of the authorized user */}
            <span>{originalName}</span>
        </div>
    )
}