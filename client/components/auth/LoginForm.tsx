import React from 'react';
import {setCookie} from "nookies";
import styles from './Auth.module.scss';
import { Form, Input, Button, notification } from 'antd';
import { LoginFormDTO } from '@/api/dto/auth.dto';

import * as Api from '../../api';

//the authorization component of the registered user
export const LoginForm: React.FC = ({}) => 
{
    //event handler for the authorization process of the registered user
    const onSubmit = async (values: LoginFormDTO) => {
        try {
            //we get a ready token obtained as a result of the implementation of the specified authorization process
            const {token} = await Api.auth.login(values);

            //a reminder appears that the request was carried out successfully
            notification.success({
                message: "Successfully!",
                description: "Go to the admin panel...",
                duration: 2
            });

            //setting the received token in cookies
            setCookie(null, "_token", token, {
                path: "/"
            });

            //we carry out the transition to the main page of the project
            location.href = "/dashboard";
        } 
        catch (err) 
        {
            //a reminder that the ongoing request failed
            notification.error({
                message: "Error!",
                description: "Invalid login or password",
                duration: 2
            })
        }
    }

    return (
        <div className={styles.formBlock}>
            <Form
                name='basic'
                labelCol={{
                    span: 8
                }}
                onFinish={onSubmit}
            >
                {/*The field of the authorized user's email task */}
                <Form.Item
                    label="E-Mail"
                    name="email"
                    rules={[
                        {
                            required: true,
                            message:  "Specify the email address"
                        }
                    ]}
                >
                    <Input/>
                </Form.Item>

                {/*Field for setting the password of the authorized user*/}
                <Form.Item
                    label="Password"
                    name="password"
                    rules={[
                        {
                            required: true,
                            message: "Specify the password"
                        }
                    ]}
                >
                    <Input.Password/>
                </Form.Item>

                <Form.Item
                    wrapperCol= {{
                        offset: 8,
                        span: 16
                    }}
                >
                    {/*Button to start the user authorization event handler*/}
                    <Button type="primary" htmlType="submit">
                        Enter
                    </Button>
                </Form.Item>
            </Form>
        </div>
    )
}