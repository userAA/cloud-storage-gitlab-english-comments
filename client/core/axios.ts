import axios from "axios";
import { parseCookies} from "nookies";

//server address
axios.defaults.baseURL = "http://localhost:7777";

axios.interceptors.request.use((config) => {
    if (typeof window !== "undefined")
    {
        //pulling the token of the authorized user from Cookies
        const { _token} = parseCookies();

        //in headers, enter token
        config.headers.Authorization = "Bearer " + _token;
    }

    return config;
})

//exporting the final axios
export default axios;