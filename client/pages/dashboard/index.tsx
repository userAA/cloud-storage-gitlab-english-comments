import {GetServerSidePropsContext, NextPage} from "next";
//importing the function of verifying the existence of an authorized user
import { checkAuth } from "@/utils/checkAuth";
import React from "react";
//importing the full web page template
import { Layout } from "../../layouts/Layout";
//importing a component of operations with files of an authorized user
import { DashboardLayout } from "@/layouts/DashboardLayout";

import * as Api from "@/api";
//pulling out the data template by files
import {fileItem} from "@/api/dto/files.dto";
//we pull out the component of displaying files of an authorized user
import { Files } from "@/modules/Files";

//forming the props template of the main project page
interface Props {
    items: fileItem[];
}

//template of the main project page
const DashboardPage: NextPage<Props> = ({items}) => {
    return  (
        //component of operations with files of an authorized user
        <DashboardLayout>
            {/*The component for displaying files of an authorized user*/}
            <Files items={items} withActions/>
        </DashboardLayout>
    )
}

DashboardPage.getLayout = (page: React.ReactNode) => {
    //setting the title of the main page of the project
    return <Layout title="Dashboard / Main">{page}</Layout>
}

//the function of getting props on the main page of the project
export const getServerSideProps = async (ctx: GetServerSidePropsContext) => {
    //we receive data on the authorized user
    const authProps = await checkAuth(ctx);

    //if there is a redirect in the received data, then go to the user registration and authorization page
    if ('redirect' in authProps) {
        return authProps;
    }

    try {
        //we get all the files of the authorized user
        const items = await Api.files.getAll();

        return {
            props: {
                //forming the desired props
                items
            }
        }
    }
    catch (err) {
        console.log(err);
        //in case of an unsuccessful request
        return {
            props: {items : []}
        }
    }
}

export default DashboardPage;