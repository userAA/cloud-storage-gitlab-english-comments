import {GetServerSidePropsContext, NextPage} from "next";
//importing the function of verifying the existence of an authorized user
import { checkAuth } from "@/utils/checkAuth";
import React from "react";
//importing the full web page template
import { Layout } from "../../layouts/Layout";

import * as Api from "@/api";
//pulling out the data template by files
import {fileItem} from "@/api/dto/files.dto";
//importing a component of operations with files of an authorized user
import { DashboardLayout } from "@/layouts/DashboardLayout";
//we pull out the component of displaying files of an authorized user
import { Files } from "@/modules/Files";

//we form the props template of the page of files deleted by an authorized user
interface Props {
    items: fileItem[];
}

//template for the page showing files deleted by an authorized user
const DashboardTrash: NextPage<Props> = ({items}) => {
    return  (
        //component of operations with files of an authorized user
        <DashboardLayout>
            {/*Component of files deleted by authorized users*/}
            <Files items={items} />
        </DashboardLayout>
    )
}

DashboardTrash.getLayout = (page: React.ReactNode) => {
    //setting the title of the page for showing files deleted by an authorized user
    return <Layout title="Dashboard / Trash">{page}</Layout>
}

//the function of getting props on the page showing files deleted by an authorized user
export const getServerSideProps = async (ctx: GetServerSidePropsContext) => {
    //check if an authorized user exists
    const authProps = await checkAuth(ctx);

    //if not, we redirect to the authorization page of the registered user
    if ('redirect' in authProps) {
        return authProps;
    }

    try 
    {
        //we get all files deleted by authorized users
        const items = await Api.files.getAll("trash");

        return {
            //forming the desired props
            props: {
                items
            }
        }
    }
    catch (err) {
        console.log(err);
        //in case of an unsuccessful request
        return {
            props: {items : []}
        }
    }
}

export default DashboardTrash;