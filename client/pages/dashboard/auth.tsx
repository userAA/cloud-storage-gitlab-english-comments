import {NextPage} from 'next';
import Head from 'next/head';
//pulling out the user authorization component
import {LoginForm} from '../../components/auth/LoginForm';
//pulling out the user registration component
import {RegisterForm} from '../../components/auth/RegisterForm';

import {Tabs} from 'antd'

//user registration and authorization page
const AuthPage: NextPage = () => {
    return (
        <>
            {/*Page title*/}
            <Head>
                <title>Dashboard / Auth</title>
            </Head>
            <main style={{width: 400, margin: '50px auto'}} >
                <Tabs
                    items = {[
                        //user authorization key 
                        {
                            label: 'Enter',
                            key: '1',
                            children: <LoginForm/>
                        },
                        //user registration key
                        {
                            label : 'Register',
                            key: '2',
                            children: <RegisterForm/>
                        }
                    ]} 
                />
            </main>
        </>
    )
}

export default AuthPage;