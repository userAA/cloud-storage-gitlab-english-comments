import {GetServerSidePropsContext, NextPage} from "next";
//importing the function of verifying the existence of an authorized user
import { checkAuth } from "@/utils/checkAuth";
import React from "react";
//importing the full web page template
import { Layout } from "../../layouts/Layout";
//importing a component of operations with files of an authorized user
import { DashboardLayout } from "../../layouts/DashboardLayout";

import * as Api from "@/api";
//pulling out the data template by files
import {fileItem} from "@/api/dto/files.dto";
//we pull out the component of displaying files of an authorized user
import { Files } from "@/modules/Files";

//we form the props template of the page for displaying photos of an authorized user
interface Props {
    items: fileItem[];
}

//template for the authorized user's photo display page
const DashboardPhotos: NextPage<Props> = ({items}) => {
    return  (
        //component of operations with files of an authorized user
        <DashboardLayout>
            {/*The component of displaying photos of an authorized user */}
            <Files items={items} withActions/>
        </DashboardLayout>
    )
}

DashboardPhotos.getLayout = (page: React.ReactNode) => {
    //setting the title of the page for displaying photos of an authorized user
    return <Layout title="Dashboard / Фото">{page}</Layout>
}

//the function of getting props on the page of showing photos of an authorized user
export const getServerSideProps = async (ctx: GetServerSidePropsContext) => {
    //check if an authorized user exists
    const authProps = await checkAuth(ctx);

    //if not, we redirect to the authorization page of the registered user
    if ('redirect' in authProps) {
        return authProps;
    }

    try 
    {
        //we get all the photos of the authorized user
        const items = await Api.files.getAll("photos");

        return {
            //forming the desired props
            props: {
                items
            }
        }
    }
    catch (err) {
        console.log(err);
        //in case of an unsuccessful request
        return {
            props: {items : []}
        }
    }
}

export default DashboardPhotos;