import {GetServerSidePropsContext, NextPage} from "next";
//importing a template of information about an authorized user
import {User} from '../../api/dto/auth.dto';

import {Button} from "antd";
import {Layout} from "../../layouts/Layout"

import styles from '@/styles/Profile.module.scss'
//importing the function of verifying the existence of an authorized user
import {checkAuth} from "../../utils/checkAuth"; 
import * as Api from "../../api";

//template for information about an authorized user
interface Props {
    userData: User;
}

//page for displaying information about an authorized user
export const DashboardProfilePage: NextPage<Props> = ({userData}) => {

    //handler for the event of deleting from the cookie token of an authorized user
    const onClickLogout = () => {
        if (window.confirm("Do you really want to get out?"))
        {
            //removing the user authorization token from the cookie
            Api.auth.logout();
            //go to the central page of the project
            location.href = "/dashboard";
        }
    }

    return (
        <main>
            <div className={styles.root}>
                <h1>My profile</h1>
                <br />
                <p>
                    {/*ID of the authorized user */}
                    ID: <b>{userData.id}</b>
                </p>
                <p>
                    {/*Full name of the authorized user*/}
                    Full name: <b>{userData.fullName}</b>
                </p>
                <p>
                    {/*Email of the authorized user*/}
                    E-Mail: <b>{userData.email}</b>
                </p>
                <br/>
                {/*The button for removing authorization from an authorized user*/}
                <Button onClick={onClickLogout} type="primary" danger>
                    Escape                    
                </Button>
            </div>
        </main>
    )
}

DashboardProfilePage.getLayout = (page: React.ReactNode) => {
    //setting the pages for displaying information about the authorized user
    return <Layout title="Dashboard / Profile" >{page}</Layout>
}

//the function of getting props on the page showing the data of an authorized user
export const getServerSideProps = async (ctx: GetServerSidePropsContext) => {
    //check if an authorized user exists
    const authProps = await checkAuth(ctx);

    //if not, we redirect to the authorization page of the registered user
    if ("redirect" in authProps) {
        return authProps;
    }

    //we make a request to receive all the data on the authorized user
    const userData = await Api.auth.getMe();

    return {
        //forming the desired props
        props: {
            userData
        }
    }
}

export default DashboardProfilePage;