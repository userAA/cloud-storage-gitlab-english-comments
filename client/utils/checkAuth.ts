import { GetServerSidePropsContext } from "next";
import nookies from "nookies";
import axios from "../core/axios";
import * as Api from "../api";

//the function of verifying the existence of user authorization
export const checkAuth = async (ctx: GetServerSidePropsContext) => {

    //we pull the token of an authorized user from cookies if such a user exists
    const {_token} = nookies.get(ctx);

    //upload the token received from nookies to axios
    axios.defaults.headers.Authorization = "Bearer " + _token;

    try {
        //we send a request to get all the data for an authorized user, if such a user exists
        await Api.auth.getMe();

        return {
            //the specified request was executed successfully
            props: {}
        };
    } catch (err) {
        return {
            //the specified request failed, we redirect to the authorization page of the registered user
            redirect: {
                destination: "/dashboard/auth",
                permanent: false
            }
        }
    }
}