import { Extension } from "./getColorByExtension";

//file extension detection function
export const getExtensionFromFileName = (filename: string) => {
    return filename.split(".").pop() as Extension;
}