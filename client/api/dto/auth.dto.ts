//data interface for authorization of a registered user
export interface LoginFormDTO {
    email: string;
    password: string;
}

//interface of data on the result of authorization of the registered user
export interface LoginResponseDTO {
    token: string;
}

//data interface for user registration
export type RegisterFormDTO = LoginFormDTO & {fullName: string};

//user registration result data interface
export type RegisterResponseDTO = LoginResponseDTO;

//interface of registered and authorized user data
export interface User {
    id: number;
    email: string;
    fullName: string;
}