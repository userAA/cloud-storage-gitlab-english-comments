import {User} from "@/api/dto/auth.dto";

//template of complete information about the file
export interface fileItem {
    filename: string;
    originalName: string;
    size: number;
    mimetype: string;
    user: User;
    deleteAt: string | null;
    id: number;
}