import axios from "../core/axios";
import {
    //вытаскиваем интерфейс данных для авторизации зарегистрированного пользователя
    //we pull out the data interface for authorization of the registered user
    LoginFormDTO, 
    //вытаскиваем интерфейс данных о результате авторизации зарегистрированного пользователя
    //we pull out the data interface about the result of the authorization of the registered user
    LoginResponseDTO,
    //вытаскиваем интерфейс данных для регистрации пользователя
    //pulling out the data interface for user registration
    RegisterFormDTO,
    //вытаскиваем интерфейс данных о результате регистрации пользователя
    //we pull out the data interface about the result of user registration
    RegisterResponseDTO,
    //вытаскиваем интерфейс данных о зарегистрированном и авторизованном пользователе
    //we pull out the interface of data about the registered and authorized user
    User
} from "../api/dto/auth.dto";
import {destroyCookie} from "nookies";

//the function of the request for authorization of the registered user
export const login = async (values : LoginFormDTO): Promise<LoginResponseDTO> => {
    //request for authorization of a registered user
    return ( await axios.post("/auth/login", values)).data;
};

//user registration request function
export const register = async (values : RegisterFormDTO): Promise<RegisterResponseDTO> => {
    //user registration request
    return ( await axios.post("/auth/register", values)).data;
};
 
//the function of requesting full information about the authorized user
export const getMe = async(): Promise<User> => {
    //request for full information about the authorized user
    return (await axios.get("/users/me")).data;
};

//the function of removing information about an authorized user from cookies
export const logout = () => {
    destroyCookie(null, "_token", {path: "/"});
};