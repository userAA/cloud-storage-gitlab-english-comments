//exporting user-related queries
export * as auth from "./auth";
//exporting requests related to files
export * as files from "./files";