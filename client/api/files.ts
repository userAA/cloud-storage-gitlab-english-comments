import axios from "@/core/axios";
//pulling out the data template by file
import {fileItem} from "@/api/dto/files.dto";

//the file types used are either all or photos or marked as deleted
type FileType = "all" | "photos" | "trash";

//the function of a request to receive all files of an authorized user from the server
export const getAll = async(type: FileType = "all"): Promise<fileItem[]> => {
    return (await axios.get("/files?type=" + type)).data;
}

//request function for marking the selected file as deleted
export const remove = (ids: number[]): Promise<void> => {
    return axios.delete("/files?ids=" + ids);
};

//the function of downloading a file from disk and adding it to the server
//(this is all done by an authorized user)
export const uploadFile = async (options: any) => {
    const { onSuccess, onError, file, onProgress} = options;

    const formData = new FormData();
    formData.append("file", file);

    const config = {
        headers: {"Content-Type": "multipart/form-data"},
        //tracking the process of downloading a file from disk
        onProgress: (event: ProgressEvent) => {
            onProgress({percent: (event.loaded / event.total)*100})
        }
    };

    try {
        //making a request to add a file downloaded from disk to the server
        const {data} = await axios.post("files", formData, config);

        //the specified request was executed successfully
        onSuccess();

        return data;
    } 
    catch (err)
    {
        //the specified request failed
        onError({err});
    }
}
