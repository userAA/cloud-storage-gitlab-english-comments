import React from "react";
import {fileItem} from "@/api/dto/files.dto";
import {FileActions} from "@/components/FileActions";
import {FileList, FileSelectType} from "@/components/FileList";
import { Empty} from "antd";

import * as Api from "@/api";

interface FilesProps {
    items: fileItem[];
    withActions?: boolean;
}

//component of the content of the authorized user's files
export const Files: React.FC<FilesProps> = ({items, withActions}) => {
    //status of the list of files sent to the trash by an authorized user
    const [files, setFiles] = React.useState(items || []);
    //status of the list of identifiers for the selected files of the authorized user
    const [selectedIds, setSelectedIds] = React.useState<number[]>([]);

    //the function of allocating files of an authorized user
    const onFileSelect = (id: number, type: FileSelectType) => {
        if (type === "select")
        {
            //the files are selected by creating a frame
            setSelectedIds((prev) => [...prev, id]);
        }
        else
        {
            //files are selected by clicking on the file
            setSelectedIds((prev) => prev.filter((_id) => _id !== id));
        }
    } 

    //the function of marking selected files as deleted
    const onClickRemove = () => {
        //the list of ids of the selected files is being cleared
        setSelectedIds([]);
        //creating a list of files to be sent to the trash
        setFiles((prev) => prev.filter((file) => !selectedIds.includes(file.id)));
        //we make a request to send the corresponding list of files to the trash
        Api.files.remove(selectedIds as any);
    }
    
    return (
        <div>
            {files.length ? (
                <>
                    {withActions && 
                        //компонент отправки выделенных файлов в корзину
                        <FileActions
                            onClickRemove={onClickRemove}
                            isActive={selectedIds.length > 0}
                        />
                    }
                    {/*Сам список файлов авторизованного пользователя с функцией их выделения */}
                    <FileList items={files} onFileSelect={onFileSelect}/>
                </>
            ) : (
                <Empty className="empty-block" description="The file list is empty" />    
            )}
        </div>
    )
}