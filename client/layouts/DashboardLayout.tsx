import React from "react";
import styles from "@/styles/Home.module.scss";
//pulling out the router hook
import { useRouter } from "next/router";
//we pull out the file download button from the disk
import {UploadButton} from "@/components/UploadButton";
import {Menu} from "antd";
import {
    DeleteOutlined,
    FileImageOutlined,
    FileOutlined
} from "@ant-design/icons";

//component of operations with files of an authorized user
export const DashboardLayout: React.FC<React.PropsWithChildren> = ({
   children 
}) => {
    //we form a router from the existing router hook
    const router = useRouter();

    //we determine the name of the key of the selected component of the display menu of all files
    const selectedMenu = router.pathname;

    return (
        <main className={styles.dashboardContainer}>
            <div className={styles.sidebar}>
                {/*Button for uploading a new file */}
                <UploadButton />
                <Menu 
                    className={styles.menu}
                    mode="inline"
                    selectedKeys={[selectedMenu]}
                    items={[
                        //The key for displaying all files of an authorized user
                        {
                            key: `/dashboard`,
                            icon: <FileOutlined />,
                            label: `Files`,
                            //event of switching on the display transition router all uploaded files by an authorized user
                            onClick: () => router.push("/dashboard")
                        },
                        //The key for displaying image files of an authorized user
                        {
                            key: `/dashboard/photos`,
                            icon: <FileImageOutlined />,
                            label: `Photos`,
                            //event of switching on the display transition router all uploaded image files by an authorized user
                            onClick: () => router.push("/dashboard/photos")
                        },
                        //The key for displaying files placed by an authorized user in the trash
                        {
                            key: `/dashboard/trash`,
                            icon: <DeleteOutlined />,
                            label: `Trash`,
                            //event of switching on the display transition router
                            //all files placed in the trash by an authorized user
                            onClick: () => router.push("/dashboard/trash")
                        },
                    ]}
                />
            </div>

            {/*File display component (files can be selected, placed in the trash and only images can be selected from them)*/}
            <div className="container">{children}</div>
        </main>
    )
}