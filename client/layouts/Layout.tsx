import Head from"next/head"
import { Header } from "@/components/Header"
import React from "react"

import styles from "@/styles/Home.module.scss";

interface LayoutProps {
    title: string;
}

//web page template
export const Layout: React.FC<React.PropsWithChildren<LayoutProps>> = ({
    //name of the web page  
    title,
    //components of the main part of the web page
    children
}) => {
    return (
        <>
            {/*Title of the web page*/}
            <Head>
                <title>{title}</title>
            </Head>
            <main>
                {/*The main part of the web page*/}
                <Header />
                {/*The main part of the web page*/}
                <div className={styles.main}>
                    <div className={styles.layout}>{children}</div>
                </div>        
            </main>
        </>
    )
}