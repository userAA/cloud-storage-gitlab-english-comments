import {
    Column, 
    DeleteDateColumn, 
    Entity, 
    PrimaryGeneratedColumn, 
    ManyToOne
} from 'typeorm'
import { UserEntity } from 'src/users/entities/user.entity';

//file types
export enum FileType {
    //photos
    PHOTOS = 'photos',
    //files in the trash
    TRASH = 'trash'
}

@Entity('files')
//database schema by file
export class FileEntity {
    //file ID
    @PrimaryGeneratedColumn()
    id: number;

    //generated file name
    @Column()
    filename: string;

    //original file name
    @Column()
    originalName: string;

    //file size
    @Column()
    size: number;

    //file type
    @Column()
    mimetype: string;

    //the user who uploaded the file (multiple files can correspond to one user)
    @ManyToOne(() => UserEntity, (user: UserEntity) => user.files)
    user: UserEntity;

    //the date when the file was placed in the trash, if it took place
    @DeleteDateColumn()
    deletedAt?: Date;
}
