import { Injectable } from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {FileEntity, FileType} from './entities/file.entity';
import {Repository} from 'typeorm'

@Injectable()
//class of operations with files
export class FilesService {
  constructor(
    @InjectRepository(FileEntity)
    private repository: Repository<FileEntity>
  ) {}

  //the function of getting user files with the user Id
  findAll(userId: number, fileType: FileType) {
    const qb = this.repository.createQueryBuilder('file');

    //we get all the specified files
    qb.where('file.userId = :userId', { userId });

    if (fileType === FileType.PHOTOS) {
      //among all the received files, we select only those that are images
      qb.andWhere('file.mimetype ILIKE :type', { type: '%image%' });
    }

    if (fileType === FileType.TRASH) {
      //among the received files, we select only those that are in the trash
      qb.withDeleted().andWhere('file.deletedAt IS NOT NULL');
    }

    return qb.getMany();
  }

  //we send information about the newly created file to the database
  create(file: Express.Multer.File, userId: number)
  {
    return this.repository.save({
      //generated file name
      filename: file.filename,
      //original file name
      originalName: file.originalname,
      //file size
      size: file.size,
      //file type (text or pixel)
      mimetype: file.mimetype,
      //id of the user who downloaded this file from disk
      user: {id : userId}
    });
  }

  //the function of placing a series of user files with the userId identifier in the trash
  async remove(userId: number, ids: string)
  {
    //we select the ids of the files that need to be placed in the trash
    const idsArray = ids.split(',');
    
    const qb = this.repository.createQueryBuilder('file');

    //we perform the specified process
    qb.where('id IN (:...ids) AND userId = :userId', {
      ids: idsArray,
      userId
    });

    //we output the result of the operation
    return qb.softDelete().execute();
  }
}
