import { diskStorage} from 'multer';

//функция определения идентификатора файла
const generateId = () => Array(18).fill(null).map(() => Math.round(Math.random() * 16).toString(16)).join('');

//function for determining the new file name
const normalizeFileName = (__: any, file: any, callback: any) => {
    const fileExtName = file.originalname.split('.').pop();

    callback(null, `${generateId()}.${fileExtName}`);
};

//the function of creating a file in the upload project folder
export const fileStorage = diskStorage({
    destination: './uploads',
    filename: normalizeFileName
})