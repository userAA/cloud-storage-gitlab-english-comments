import { 
  Controller, 
  ParseFilePipe, 
  Post, 
  Get,
  UploadedFile, 
  UseInterceptors,
  MaxFileSizeValidator,
  UseGuards,
  Query,
  Delete
} from '@nestjs/common';
import { FilesService } from './files.service';
import {ApiBearerAuth, ApiConsumes, ApiTags, ApiBody } from '@nestjs/swagger';
import { FileInterceptor } from '@nestjs/platform-express';
//we pull out the function of saving the file to the upload folder
import { fileStorage } from './storage';
import { JwtAuthGuard } from 'src/auth/guards/jwt.guard';
//we determine the ID of the authorized user
import { UserId } from '../decorators/user-id.decorator';
//pulling out file types
import { FileType } from './entities/file.entity';

@Controller('files')
@ApiTags('files')
//checking user authorization
@UseGuards(JwtAuthGuard)
//information about the authorized user's token is stored here
@ApiBearerAuth()
export class FilesController {
  constructor(private readonly filesService: FilesService) {}

  //request to receive files downloaded from disk by an authorized user
  @Get()
  findAll(
    //идентификатор авторизованного пользователя
    @UserId() userId: number, 
    //file type
    @Query("type") fileType: FileType
  ) {
    //we receive files downloaded from the disk by authorized users
    return this.filesService.findAll(userId, fileType);
  }

  //request to add complete information about a new file downloaded from disk to the database
  @Post()
  @UseInterceptors(
    FileInterceptor('file', {
      //the function of saving a file in the project folder
      storage: fileStorage
    })
  )
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary'
        }
      }
    }
  })
  create(@UploadedFile(
    new ParseFilePipe({
      //a file uploaded by an authorized user must weigh no more than five megabytes
      validators: [ new MaxFileSizeValidator ({ maxSize: 1024*1024*5})]
    })
  ) 
    //information about the new file of the authorized user
    file: Express.Multer.File,
    //ID of the authorized user
    @UserId() userId: number,
  ) {
    //adding to the database full information about the file that is downloaded from the disk by an authorized user
    return this.filesService.create(file, userId);
  }

  //request to place a certain group of files of an authorized user in the trash
  @Delete()
  remove
    //ID of the authorized user
    (@UserId() userId: number, 
    //sequence of file IDs to be placed in the trash
    @Query('ids') ids: string
  ) {
    //we put a certain group of files of an authorized user in the trash
    return this.filesService.remove(userId, ids);
  }
}
