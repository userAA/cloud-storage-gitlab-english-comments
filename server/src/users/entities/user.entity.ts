import {Column, Entity, PrimaryGeneratedColumn, OneToMany} from 'typeorm';
import { FileEntity } from 'src/files/entities/file.entity'; 

@Entity('users')
//database schema by user
export class UserEntity {
    //user ID
    @PrimaryGeneratedColumn()
    id: number;

    //user's email address
    @Column()
    email: string;

    //user password
    @Column()
    password: string;

    //full user name
    @Column()
    fullName: string;

    //link to files (one user can contain many files)
    @OneToMany( () => FileEntity, (file : FileEntity) => file.user)
    files: FileEntity[];
}