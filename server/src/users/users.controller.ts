import { Controller, Get, UseGuards } from '@nestjs/common';
import { UsersService } from './users.service';
import { ApiBearerAuth, ApiTags} from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/guards/jwt.guard';
import { UserId } from 'src/decorators/user-id.decorator';

@Controller('users')
@ApiTags('users')
@ApiBearerAuth()
export class UsersController {
  constructor(private usersService: UsersService) {}

  //request to find complete information about the authorized user
  @Get('/me')
  @UseGuards(JwtAuthGuard)
  //contacting the function JwtStrategy.validate, to get an authorized user token
  getMe(@UserId() id: number) {
    //we find the necessary information by the ID of the authorized user
    return this.usersService.findById(id);
  }
}
