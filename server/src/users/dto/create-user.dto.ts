import { ApiProperty} from '@nestjs/swagger'

//user data template
export class CreateUserDto {
    //user's email address
    @ApiProperty({
        default: 'test@test.ru'
    })
    email: string;

    //full user name
    @ApiProperty({
        default: 'Мистер Кредо'
    })
    fullName: string;

    //user password
    @ApiProperty({
        default: '123'
    })
    password: string;
}
