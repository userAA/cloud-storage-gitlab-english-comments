import { ForbiddenException, Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { UserEntity } from 'src/users/entities/user.entity';
import {JwtService} from '@nestjs/jwt'

@Injectable()
export class AuthService {
    constructor(
        //service according to user data
        private usersService: UsersService,
        //token generation service
        private jwtService: JwtService
    ) {}

    //the function of checking the existence of a user by email and password
    async validateUser(email: string, password: string) : Promise<any> {
        //we are looking for a user by email in the user database
        const user = await this.usersService.findByEmail(email);

        //if the user is found, then we compare his password with the input password
        if (user && user.password === password)
        {
            const {password, ...result} = user;
            //if successful, we return the desired result
            return result;
        }

        //otherwise, a null result is returned
        return null;
    }

    //new user registration function
    async register(dto: CreateUserDto)
    {
        try
        {
            //we carry out the specified registration
            const userData = await this.usersService.create(dto);
            return userData;
        } 
        catch (err)
        {
            console.log(err);
            throw new ForbiddenException('Error during registration');
        }
    }

    //the authorization function of the registered user
    //(there is a connection with the function JwtStrategy.validate )
    async login(user: UserEntity) {
        return {
            //we get the authorization token of the registered user
            token: this.jwtService.sign({id: user.id})
        }
    }
}
