import { Body, Controller, Post, Request, UseGuards } from '@nestjs/common';
import { ApiBody} from '@nestjs/swagger';
import { CreateUserDto} from '../users/dto/create-user.dto';
import { AuthService } from './auth.service';
import { UserEntity } from 'src/users/entities/user.entity';
import { LocalAuthGuard } from './guards/local.guard';

@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService) {}

    //we check whether there is a registered user who needs to be authorized 
    //(at the same time, we contact the function LocalStrategy.validate) 
    @UseGuards(LocalAuthGuard)
    //we are making a request for authorization of a registered user
    @Post('/login')
    //setting the input data template for the authorization of the registered user
    @ApiBody({type: CreateUserDto})
    login(@Request() req: any) 
    {
        //we carry out the required authorization
        return this.authService.login(req.user as UserEntity);
    }

    //request to register a new user
    @Post('/register')
    register(@Body() dto: CreateUserDto) {
        //we carry out the required registration
        return this.authService.register(dto);
    }
}
