import { Strategy} from 'passport-local';
import {PassportStrategy} from '@nestjs/passport';
import {Injectable, UnauthorizedException} from '@nestjs/common';
import { AuthService } from '../auth.service';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
    constructor(private authService: AuthService) {
        super({
            usernameField: 'email'
        });
    }

    //the function of checking the existence of a registered user by email and password
    async validate(email: string, password: string) : Promise<any> {
        //looking for the specified user
        const user = await this.authService.validateUser(email, password);

        if (!user) 
        {
            //couldn't find the user
            throw new UnauthorizedException('Invalid username or password');
        }

        //the user exists
        return user;
    }
}